package com.ubitricity.codechallenge.rest;

import com.ubitricity.codechallenge.manager.StationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class ReportService {

    private StationManager stationManager;

    @Autowired
    public ReportService( StationManager stationManager){
        this.stationManager = stationManager;
    }

    @GetMapping("/report")
    public String report(){
        return stationManager.getReport();
    }

}
