package com.ubitricity.codechallenge.rest;

import com.ubitricity.codechallenge.exception.ChargePointIsBusyException;
import com.ubitricity.codechallenge.manager.StationManager;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/charge-point")
public class ChargePointService {

    private StationManager stationManager;

    public ChargePointService( StationManager stationManager){
        this.stationManager = stationManager;
    }

    @PostMapping("/plugged")
    public void plugged(@RequestBody int chargePointNumber) throws ChargePointIsBusyException {
        stationManager.plugged(chargePointNumber);
    }

    @PostMapping("/unplugged")
    public void unplugged(@RequestBody  int chargePointNumber){
        stationManager.unplugged(chargePointNumber);
    }
}
