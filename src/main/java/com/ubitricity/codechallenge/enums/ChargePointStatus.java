package com.ubitricity.codechallenge.enums;

public enum ChargePointStatus {
    AVAILABLE, OCCUPIED
}
