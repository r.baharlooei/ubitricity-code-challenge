package com.ubitricity.codechallenge.repository;

import com.ubitricity.codechallenge.enums.ChargePointStatus;
import com.ubitricity.codechallenge.manager.StationManager;
import com.ubitricity.codechallenge.model.ChargePointModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChargePointRepository {

    private static List<ChargePointModel> stationOccupiedChargePoints = new ArrayList<>();

    public void addChargePointToOccupiers(ChargePointModel chargePoint){
        stationOccupiedChargePoints.add(chargePoint);
    }

    public void removeChargePointFromOccupiersByNumber(int chargePointNumber){
        stationOccupiedChargePoints.removeIf(cp -> cp.getChargePointNumber() == chargePointNumber);
    }

    public boolean isChargePointBusy(int chargePointNumber){
        return stationOccupiedChargePoints.stream().filter(cp -> cp.getChargePointNumber() == chargePointNumber).findFirst().isPresent();
    }

    public boolean areAllOtherChargePointsOccupied(){
        return stationOccupiedChargePoints.size() == 9;
    }

    public void switchEarliestCarToSlowCharge() {
        for (ChargePointModel model : stationOccupiedChargePoints) {
            if (model.getConsumedCurrent() == StationManager.fastChargeCurrent) {
                model.setConsumedCurrent(StationManager.slowChargeCurrent);
                break;
            }
        }
    }

    public boolean fastChargeAvailable() {
        int totalConsumingCurrent = stationOccupiedChargePoints.stream().mapToInt(ChargePointModel::getConsumedCurrent).sum();
        int remainingCurrent = StationManager.stationCurrentInput - totalConsumingCurrent;
        if (remainingCurrent >= StationManager.fastChargeCurrent) {
            return true;
        }
        return false;
    }

    public ChargePointModel getChargePointByNumber(int chargePointNumber){
        return stationOccupiedChargePoints.stream().filter
                (cp -> cp.getChargePointNumber() == chargePointNumber)
                .findFirst().orElse(new ChargePointModel(chargePointNumber, ChargePointStatus.AVAILABLE, 0 ));
    }

    public boolean isThereSlowChargePoint(){
        return stationOccupiedChargePoints.stream().filter(cp -> cp.getConsumedCurrent() == StationManager.slowChargeCurrent)
                .findFirst().isPresent();
    }

    public void switchLatestCarToFastCharge(){
        for (int i=stationOccupiedChargePoints.size(); i-- > 0;){
            if (stationOccupiedChargePoints.get(i).getConsumedCurrent() == StationManager.slowChargeCurrent){
                stationOccupiedChargePoints.get(i).setConsumedCurrent(StationManager.fastChargeCurrent);
                break;
            }
        }

    }



}
