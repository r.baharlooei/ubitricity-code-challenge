package com.ubitricity.codechallenge.manager;

import com.ubitricity.codechallenge.enums.ChargePointStatus;
import com.ubitricity.codechallenge.exception.ChargePointIsBusyException;
import com.ubitricity.codechallenge.model.ChargePointModel;
import com.ubitricity.codechallenge.repository.ChargePointRepository;
import org.springframework.stereotype.Service;


@Service
public class StationManager {

    private final ChargePointRepository chargePointRepository;

    public StationManager(ChargePointRepository chargePointRepository) {
        this.chargePointRepository = chargePointRepository;
    }

    public final static int stationCurrentInput = 100;
    public final static int fastChargeCurrent = 20;
    public final static int slowChargeCurrent = 10;



    public void plugged(int chargePointNumber) throws ChargePointIsBusyException {
        if (chargePointRepository.isChargePointBusy(chargePointNumber)) {
            throw new ChargePointIsBusyException();
        }
        ChargePointModel model = new ChargePointModel();
        model.setChargePointNumber(chargePointNumber);
        model.setChargePointStatus(ChargePointStatus.OCCUPIED);

        if (chargePointRepository.areAllOtherChargePointsOccupied()){
            chargePointRepository.switchEarliestCarToSlowCharge();
            model.setConsumedCurrent(slowChargeCurrent);
        }
        else {
            while (!chargePointRepository.fastChargeAvailable()) {
                chargePointRepository.switchEarliestCarToSlowCharge();
            }
            model.setConsumedCurrent(fastChargeCurrent);
        }
        chargePointRepository.addChargePointToOccupiers(model);

    }


    public void unplugged(int chargePointNumber) {
        int releasedCurrent = chargePointRepository.getChargePointByNumber(chargePointNumber).getConsumedCurrent();
        chargePointRepository.removeChargePointFromOccupiersByNumber(chargePointNumber);
        while (releasedCurrent > 0 && chargePointRepository.isThereSlowChargePoint()){
            chargePointRepository.switchLatestCarToFastCharge();
            releasedCurrent-=(fastChargeCurrent-slowChargeCurrent);
        }

    }


    public String getReport() {
        String report = "";
        for (int i = 1; i < 11; i++) {
            report += chargePointRepository.getChargePointByNumber(i).toString();
            report+= (i == 10)? "" :"\n";
        }
        return report;
    }


}
