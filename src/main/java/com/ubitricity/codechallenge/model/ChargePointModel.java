package com.ubitricity.codechallenge.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.ubitricity.codechallenge.enums.ChargePointStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ChargePointModel {


    @NotNull
    private int chargePointNumber;

    @NotNull
    private ChargePointStatus chargePointStatus = ChargePointStatus.AVAILABLE;

    @Nullable
    private int consumedCurrent;

    @Override
    public String toString() {
        String consumedCurrentForToString = (consumedCurrent == 0) ? "": "  "+consumedCurrent+"A";
        return "CP"+chargePointNumber+"  "+chargePointStatus+consumedCurrentForToString;
    }
}
