package com.ubitricity.codechallenge.manager;

import com.ubitricity.codechallenge.exception.ChargePointIsBusyException;
import com.ubitricity.codechallenge.repository.ChargePointRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles( "test" )
public class StationManagerTest {

    @Autowired
    StationManager stationManager;

    @Autowired
    ChargePointRepository chargePointRepository;

    @Before
    public void unpluggedAllChargePoints(){
        for (int i=1 ; i<11 ; i++){
            stationManager.unplugged(i);
        }
    }


    @Test(expected = ChargePointIsBusyException.class)
    public void testPreventDuplicatePlugged() throws ChargePointIsBusyException {
        stationManager.plugged(1);
        stationManager.plugged(1);
    }


    @Test
    public void switchToFastChargeInUnplug() throws ChargePointIsBusyException {
        stationManager.plugged(1);
        stationManager.plugged(2);
        stationManager.plugged(3);
        stationManager.plugged(4);
        stationManager.plugged(5);
        stationManager.plugged(10);
        assertEquals(10, chargePointRepository.getChargePointByNumber(1).getConsumedCurrent());
        assertEquals(10, chargePointRepository.getChargePointByNumber(2).getConsumedCurrent());
        stationManager.unplugged(2);
        assertEquals(20, chargePointRepository.getChargePointByNumber(1).getConsumedCurrent());

    }


    @Test
    public void switchToSlowCharge() throws ChargePointIsBusyException {
        stationManager.plugged(1);
        stationManager.plugged(2);
        stationManager.plugged(3);
        stationManager.plugged(4);
        assertEquals(20, chargePointRepository.getChargePointByNumber(4).getConsumedCurrent());
        stationManager.plugged(5);
        stationManager.plugged(10);
        assertEquals(10, chargePointRepository.getChargePointByNumber(1).getConsumedCurrent());
        assertEquals(10, chargePointRepository.getChargePointByNumber(2).getConsumedCurrent());

    }


}
