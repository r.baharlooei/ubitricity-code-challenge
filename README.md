# ubitricity-code-challenge



## What's this

This App written for managing a group of charge point in erea that feed of common input.

## Getting started

- [ ] POST /charge-point/plugged  
	call this api when a charge point plugged to car. Based on the available current and priority App assign a current to this charge point. just give a charge point number in body.
- [ ] POST /charge-point/plugged  
	call this api when a charge point unplugged to car. If necessary based on the available current and priority App assign a more current to other charge point. just give a charge point number in body.
- [ ] GET  /report  
	this api give a status of all charge points as a text.
